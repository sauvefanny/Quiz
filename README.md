# Project Quiz

## Créer une application de type quizz/QCM/questionnaire sur le thème de votre choix.

### I. Commencer par trouver le thème du quizz et une liste de questions pour celui ci
------
J'ai commencé à chercher les différents Quiz qui pouvait exister.
Petit à petit comme j'ai changé d'idée je n'ai gardé de ma maquette qu'une partie ( la page 1, 2 et 5 ).


<img src="public/img/maquette1.png" width="100" height="100" />
<img src="public/img/maquette2.png" width="100" height="100" />
<img src="public/img/maquette3.png" width="100" height="100" />
<img src="public/img/maquette4.png" width="100" height="100" />
<img src="public/img/maquette5.png" width="100" height="100" />


J'étais partie sur le thème des révisions de ce que nous avions vu depuis le début de l'année.
Puis j'ai choisie comme cliente ma fille qui a souhaité avoir un Quizz sur le dessin animé Miraculous.
Une cliente exigente sur les couleurs et le contenu.
Je lui ai proposé un quiz avec la validation de la bonne question et elle m'a repondu que les enfants ce trompaient toutes la journée a l'école alors il fallait pouvoir changer de choix et pour les petits leur indiqués qu'il y avait une erreur.
J'ai trouvé l'idée sympa.


### II.Compétences à mobilisées
------
J'ai utilisé des variables et des `addEventListener` pour changer de contenu au `click` en manipulant  DOM (querySelector / Events).

J'ai tout d'abord construit le tableau d'objet avec mes questions et mes réponses.

ex : 
```
let containerQuestion =
    [
        {
            image: '',
            question: 'Dans quelle ville se passe "Miraculous Ladybug ?',
            choices0: 'Lyon',
            choices1: 'Marseille',
            choices2: 'Paris',
            choices3: 'Barcelone',
            correctAnswer: "choices2"
        },

```
\
 J'ai crée des boucles et des fonctions en les testant au fur et a mesure.

J'ai essayer de maintenir mon code DRY pour ce faire je suis aller rechercher dans les bonnes pratiques et je l'ai reorganisée et commentée afin qu'il soit accessible pour de future modifications.
\
1. Écrire du JavaScript dans un fichier séparé
2. JSON pour donner un comportement dynamique sans pour autant que le code JS change en soit
3. J'ai essayer d'être "minimaliste
4. J'ai inclue le Javascript en bas de page
5. utilisation de let et const
6. initialiser les variables au début du script


[lien vers mon jeux en ligne.]­(https://sauvefanny.gitlab.io/Quiz)
