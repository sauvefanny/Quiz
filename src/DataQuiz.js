export let containerQuestion =
    [
        {
            image: '',
            question: 'Dans quelle ville se passe "Miraculous Ladybug ?',
            choice0: 'Lyon',
            choice1: 'Marseille',
            choice2: 'Paris',
            choice3: 'Barcelone',
            correctAnswer: "choice2"
        },
        {
            image: '',
            question: 'Combien existe-t-il de miraculous',
            choice0: '7 , Sept',
            choice1: '19 ,dix-neuf',
            choice2: '14 ,quatorze',
            choice3: '2 , deux',
            correctAnswer: "choice1",
        },
        {
            image: '/img/question2.jpg',
            question: 'De qui Chat noir est-il amoureux ? ',
            choice0: 'Marinette',
            choice1: 'LadyBug',
            choice2: 'Chloé',
            choice3: 'Pipistrelle',
            correctAnswer: "choice2"
        },
        {
            image: '',
            question: 'Quel est le nom du méchant ?',
            choice0: 'Le Papillon',
            choice1: 'Le Papillon gris',
            choice2: 'Le Papillon noir',
            choice3: 'Le Papillon de nuit',
            correctAnswer: "choice0"

        },
        {
            image: '',
            question: 'Quels sont les miraculous de Ladybug et de Chat Noir ?',
            choice0: 'Une abeille pour Ladybug et un bâton pour Chat Noir',
            choice1: 'Des boucles d\'oreilles pour Ladybug et un bâton pour Chat Noir',
            choice2: 'Un yoyo pour Ladybug et une bague pour Chat Noir',
            choice3: ' Des boucles d\'oreilles pour Ladybug et une bague pour Chat Noir',
            correctAnswer: "choice3"
        },
        {
            image: '',
            question: 'Pourquoi Gabriel agreste akumatize les gens en papillon ?',
            choice0: 'Pour ressusciter sa femme',
            choice1: 'Par plaisir, parce qu\'il est méchant',
            choice2: 'Pour devenir riche',
            choice3: 'Autre',
            correctAnswer: "choice0"
        },
        {
            image: '',
            question: 'Quel est le prénom de la mère de Chloé ?',
            choice0: 'Sabine',
            choice1: 'Alix',
            choice2: 'Audrey',
            choice3: 'Autre',
            correctAnswer: "choice2"
        },
        {
            image: '',
            question: 'Comment est appelée Nathalie une fois akumatizée ?',
            choice0: 'Style queen',
            choice1: 'Climatika',
            choice2: 'Mayura',
            choice3: 'Catalyst',
            correctAnswer: "choice2"
        },
        {
            image: '',
            question: 'comment s\'appelle le kwami de Adrien?',
            choice0: 'Tikki',
            choice1: 'Trixx',
            choice2: 'Duusu',
            choice3: 'Plagg',
            correctAnswer: "choice3"
        },
        {
            image: 'img/question10.jpg',
            question: 'Qui se fait akumatiser?',
            choice0: 'Thomas Astruc',
            choice1: 'Je ne sais pas',
            choice2: 'Maestro',
            choice3: 'Le boulanger',
            correctAnswer: "choice2"
        },
    ];