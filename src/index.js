import { containerQuestion } from "./DataQuiz";


//utilisation de constante 
const startGame = document.querySelector('#start');
const questionElement = document.querySelector('#container-question');
const showresult = document.querySelector("#container-result");
const image = document.querySelector('#image');
const question = document.querySelector('#question');
const choice0 = document.querySelector('#choice0');
const choice1 = document.querySelector('#choice1');
const choice2 = document.querySelector('#choice2');
const choice3 = document.querySelector('#choice3');
const divMessage = document.querySelector('#message');


// initialisation des variables pour l'event qui passe au contenu suivant
let questionId = 0;
let nextQuestion = document.querySelector('#next');
let buttonRetry = document.querySelector('#retry'); 
let numeroQuestion = 1;
let score = 0;

buttonRetry.style.display = "none";
showresult.style.display = "none";
questionElement.style.display = "none";
startGame.style.display = "block";


//manipulation du DOM avec un event pour lancer le jeux
startGame.addEventListener('click', (e) => {
    
    e.preventDefault();
    startGame.style.display = 'none';
    questionElement.style.display = 'block';
    displayQuestion(questionId);

});


// @param {string || image } index 
function displayQuestion(index) {

    image.src = containerQuestion[index].image;
    question.textContent = containerQuestion[index].question;
    choice0.textContent = containerQuestion[index].choice0;
    choice1.textContent = containerQuestion[index].choice1;
    choice2.textContent = containerQuestion[index].choice2;
    choice3.textContent = containerQuestion[index].choice3;

};


//@param {contenu de la bonne réponse pour chaques questions}
//Jeux pour enfants donc possibilité de changer de réponse 
function correctAnswers(answer) {

    if (answer === containerQuestion[questionId].correctAnswer) {
        score++;
        divMessage.textContent = 'Bravo !! Appuie sur Next pour continuer';
        divMessage.style.color = 'blue';
        divMessage.style.display = "block";
        let divScore = document.querySelector('#score');
        divScore.textContent = 'score : ' + score;

    } else {
        divMessage.textContent = 'Cherche encore Vas-y tu vas y arriver !';
        divMessage.style.display = "block";
        divMessage.style.color = 'red';

    }
};


// Manipulation du DOM avec des events
choice0.addEventListener('click', () => {
    correctAnswers('choice0')
});
choice1.addEventListener('click', () => {
    correctAnswers('choice1')
});
choice2.addEventListener('click', () => {
    correctAnswers('choice2')
});
choice3.addEventListener('click', () => {
    correctAnswers('choice3')
});


nextQuestion.addEventListener('click', (e) => {

    const divNumeroQuestion = document.querySelector('#numero-question');
    e.preventDefault();
    questionId++;
    numeroQuestion++;
    divMessage.style.display = "none";

    if (containerQuestion[questionId]) {
        displayQuestion(questionId);
        divNumeroQuestion.textContent = `question: ${numeroQuestion} /10`;
    }
    else {
        displayStop();
    }

});


//contenu de la page à lancer quand l'index est vide
function displayStop() {

    questionElement.style.display = "none";
    startGame.style.display = "none";
    showresult.style.display = "block";
    buttonRetry.style.display = "block";

    let containerResult = document.querySelector('#container-result');
    
    
    //condition selon le score
    if (score > 5) {
        showresult.textContent = `Felicitation tu connais vraiment bien Miraculous tu a eu ${score} points de score!  Clique moi pour recommencer`;
    } else {
        showresult.textContent = `Alors? tu t'es bien amusé(e) Recommence si tu veux améliorer ton score qui de : ${score}   Clique moi pour recommencer`;
    };

    buttonRetry.addEventListener('click', (event) => {

        showresult.style.display = "none";
        buttonRetry.style.display = "none";
        containerResult.style.display = "none";
        questionElement.style.display = "none";
        startGame.style.display = "block";
        restartGame();

    })
};


//fonction qui permet de remettre l'index des questions à zero
//lancé au click du bouton restart

function restartGame() {
    questionId = 0;
};